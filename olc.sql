-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: olc
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add typ zanr',7,'add_typzanr'),(26,'Can change typ zanr',7,'change_typzanr'),(27,'Can delete typ zanr',7,'delete_typzanr'),(28,'Can view typ zanr',7,'view_typzanr'),(29,'Can add album interpret',8,'add_albuminterpret'),(30,'Can change album interpret',8,'change_albuminterpret'),(31,'Can delete album interpret',8,'delete_albuminterpret'),(32,'Can view album interpret',8,'view_albuminterpret'),(33,'Can add album skladba',9,'add_albumskladba'),(34,'Can change album skladba',9,'change_albumskladba'),(35,'Can delete album skladba',9,'delete_albumskladba'),(36,'Can view album skladba',9,'view_albumskladba'),(37,'Can add album',10,'add_album'),(38,'Can change album',10,'change_album'),(39,'Can delete album',10,'delete_album'),(40,'Can view album',10,'view_album'),(41,'Can add typ narodnost',11,'add_typnarodnost'),(42,'Can change typ narodnost',11,'change_typnarodnost'),(43,'Can delete typ narodnost',11,'delete_typnarodnost'),(44,'Can view typ narodnost',11,'view_typnarodnost'),(45,'Can add interpret',12,'add_interpret'),(46,'Can change interpret',12,'change_interpret'),(47,'Can delete interpret',12,'delete_interpret'),(48,'Can view interpret',12,'view_interpret'),(49,'Can add skladba',13,'add_skladba'),(50,'Can change skladba',13,'change_skladba'),(51,'Can delete skladba',13,'delete_skladba'),(52,'Can view skladba',13,'view_skladba');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(10,'intolc','album'),(8,'intolc','albuminterpret'),(9,'intolc','albumskladba'),(12,'intolc','interpret'),(13,'intolc','skladba'),(11,'intolc','typnarodnost'),(7,'intolc','typzanr'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-07-14 07:24:05.038100'),(2,'auth','0001_initial','2020-07-14 07:24:05.100843'),(3,'admin','0001_initial','2020-07-14 07:24:05.287666'),(4,'admin','0002_logentry_remove_auto_add','2020-07-14 07:24:05.337445'),(5,'admin','0003_logentry_add_action_flag_choices','2020-07-14 07:24:05.347769'),(6,'contenttypes','0002_remove_content_type_name','2020-07-14 07:24:05.389466'),(7,'auth','0002_alter_permission_name_max_length','2020-07-14 07:24:05.409694'),(8,'auth','0003_alter_user_email_max_length','2020-07-14 07:24:05.421823'),(9,'auth','0004_alter_user_username_opts','2020-07-14 07:24:05.431345'),(10,'auth','0005_alter_user_last_login_null','2020-07-14 07:24:05.450260'),(11,'auth','0006_require_contenttypes_0002','2020-07-14 07:24:05.452566'),(12,'auth','0007_alter_validators_add_error_messages','2020-07-14 07:24:05.462979'),(13,'auth','0008_alter_user_username_max_length','2020-07-14 07:24:05.488618'),(14,'auth','0009_alter_user_last_name_max_length','2020-07-14 07:24:05.516043'),(15,'auth','0010_alter_group_name_max_length','2020-07-14 07:24:05.531593'),(16,'auth','0011_update_proxy_permissions','2020-07-14 07:24:05.541433'),(17,'sessions','0001_initial','2020-07-14 07:24:05.550334'),(18,'intolc','0001_initial','2020-07-14 11:41:28.178422'),(19,'intolc','0002_auto_20200715_0757','2020-07-15 05:57:39.642682'),(20,'intolc','0003_skladba_album','2020-07-15 17:22:03.931581'),(21,'intolc','0004_remove_skladba_album','2020-07-15 19:15:52.420890'),(22,'intolc','0005_auto_20200715_2151','2020-07-15 19:53:56.338686'),(23,'intolc','0006_auto_20200715_2200','2020-07-15 20:00:24.631044');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_album`
--

DROP TABLE IF EXISTS `intolc_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) NOT NULL,
  `datum_vydani` date NOT NULL,
  `typ_zanr_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `intolc_album_typ_zanr_id_bbf10016_fk_intolc_typzanr_id` (`typ_zanr_id`),
  CONSTRAINT `intolc_album_typ_zanr_id_bbf10016_fk_intolc_typzanr_id` FOREIGN KEY (`typ_zanr_id`) REFERENCES `intolc_typzanr` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_album`
--

LOCK TABLES `intolc_album` WRITE;
/*!40000 ALTER TABLE `intolc_album` DISABLE KEYS */;
INSERT INTO `intolc_album` VALUES (2,'Hell is approaching','1990-06-13',2),(3,'l\'amour','1954-05-10',11),(4,'Nashville banjo','1948-11-30',1),(5,'Russians piano concertos','1999-10-12',13);
/*!40000 ALTER TABLE `intolc_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_albumskladba`
--

DROP TABLE IF EXISTS `intolc_albumskladba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_albumskladba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cislo_stopy` smallint(5) unsigned NOT NULL,
  `album_id` int(11) NOT NULL,
  `skladba_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `intolc_albumskladba_album_id_56ea329d_fk_intolc_album_id` (`album_id`),
  KEY `intolc_albumskladba_skladba_id_a4a9ec01_fk_intolc_skladba_id` (`skladba_id`),
  CONSTRAINT `intolc_albumskladba_album_id_56ea329d_fk_intolc_album_id` FOREIGN KEY (`album_id`) REFERENCES `intolc_album` (`id`),
  CONSTRAINT `intolc_albumskladba_skladba_id_a4a9ec01_fk_intolc_skladba_id` FOREIGN KEY (`skladba_id`) REFERENCES `intolc_skladba` (`id`),
  CONSTRAINT `intolc_albumskladba_cislo_stopy_bd6abb71_check` CHECK (`cislo_stopy` >= 0)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_albumskladba`
--

LOCK TABLES `intolc_albumskladba` WRITE;
/*!40000 ALTER TABLE `intolc_albumskladba` DISABLE KEYS */;
INSERT INTO `intolc_albumskladba` VALUES (2,1,3,31),(3,2,3,32),(4,3,3,33),(5,1,5,22),(6,2,5,23),(7,3,5,24),(8,1,4,28),(9,2,4,29),(10,3,4,30),(11,1,2,25),(12,2,2,26),(13,3,2,27);
/*!40000 ALTER TABLE `intolc_albumskladba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_interpret`
--

DROP TABLE IF EXISTS `intolc_interpret`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_interpret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) NOT NULL,
  `typ_narodnost_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `intolc_interpret_typ_narodnost_id_2e10c9f7_fk_intolc_ty` (`typ_narodnost_id`),
  CONSTRAINT `intolc_interpret_typ_narodnost_id_2e10c9f7_fk_intolc_ty` FOREIGN KEY (`typ_narodnost_id`) REFERENCES `intolc_typnarodnost` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_interpret`
--

LOCK TABLES `intolc_interpret` WRITE;
/*!40000 ALTER TABLE `intolc_interpret` DISABLE KEYS */;
INSERT INTO `intolc_interpret` VALUES (3,'Edith',5),(4,'Wabi&Miki',1),(5,'Nightwish',4),(6,'Sergey',3);
/*!40000 ALTER TABLE `intolc_interpret` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_interpret_album`
--

DROP TABLE IF EXISTS `intolc_interpret_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_interpret_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interpret_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `intolc_interpret_album_interpret_id_album_id_7d913cc5_uniq` (`interpret_id`,`album_id`),
  KEY `intolc_interpret_album_album_id_6a0f8be0_fk_intolc_album_id` (`album_id`),
  CONSTRAINT `intolc_interpret_alb_interpret_id_2f3cfaed_fk_intolc_in` FOREIGN KEY (`interpret_id`) REFERENCES `intolc_interpret` (`id`),
  CONSTRAINT `intolc_interpret_album_album_id_6a0f8be0_fk_intolc_album_id` FOREIGN KEY (`album_id`) REFERENCES `intolc_album` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_interpret_album`
--

LOCK TABLES `intolc_interpret_album` WRITE;
/*!40000 ALTER TABLE `intolc_interpret_album` DISABLE KEYS */;
INSERT INTO `intolc_interpret_album` VALUES (3,3,3),(4,4,4),(5,5,2),(6,6,5);
/*!40000 ALTER TABLE `intolc_interpret_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_skladba`
--

DROP TABLE IF EXISTS `intolc_skladba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_skladba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) NOT NULL,
  `delka` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_skladba`
--

LOCK TABLES `intolc_skladba` WRITE;
/*!40000 ALTER TABLE `intolc_skladba` DISABLE KEYS */;
INSERT INTO `intolc_skladba` VALUES (22,'opus 1','24:12'),(23,'opus 2','12:12'),(24,'opus 3','16:32'),(25,'hit 1','2:32'),(26,'hit 2','1:31'),(27,'hit 3','3:01'),(28,'song 1','1:31'),(29,'song 2','4:11'),(30,'song 3','0:32'),(31,'track 1','3:33'),(32,'track 2','2:22'),(33,'track 3','4:01');
/*!40000 ALTER TABLE `intolc_skladba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_typnarodnost`
--

DROP TABLE IF EXISTS `intolc_typnarodnost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_typnarodnost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_typnarodnost`
--

LOCK TABLES `intolc_typnarodnost` WRITE;
/*!40000 ALTER TABLE `intolc_typnarodnost` DISABLE KEYS */;
INSERT INTO `intolc_typnarodnost` VALUES (1,'Česká'),(3,'Ruská'),(4,'Finská'),(5,'Francouzká');
/*!40000 ALTER TABLE `intolc_typnarodnost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intolc_typzanr`
--

DROP TABLE IF EXISTS `intolc_typzanr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intolc_typzanr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nazev` (`nazev`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intolc_typzanr`
--

LOCK TABLES `intolc_typzanr` WRITE;
/*!40000 ALTER TABLE `intolc_typzanr` DISABLE KEYS */;
INSERT INTO `intolc_typzanr` VALUES (11,'chanson'),(1,'country'),(12,'punk'),(2,'rock'),(13,'vážná');
/*!40000 ALTER TABLE `intolc_typzanr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-15 22:17:32
