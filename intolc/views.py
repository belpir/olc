from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView
from django.urls import reverse_lazy
from .models import *


# Create your views here.
class IndexView(TemplateView):
    template_name = 'inolc/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album_query'] = Album.objects.all().order_by('nazev')
        return context


class TypZanrCreate(CreateView):
    model = TypZanr
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all().order_by('nazev')
        context['delete'] = f'{__class__.name}-delete'
        return context


class TypZanrDelete(DeleteView):
    model = TypZanr
    template_name = 'intolc/delete.html'
    success_url = reverse_lazy(model.__name__.casefold())


class AlbumCreate(CreateView):
    model = Album
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all().order_by('nazev')
        context['delete'] = f'{__class__.name}-delete'
        return context


class AlbumDelete(DeleteView):
    model = Album
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = reverse_lazy(model.__name__.casefold())


class SkladbaCreate(CreateView):
    model = Skladba
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all().order_by('nazev')
        context['delete'] = f'{__class__.name}-delete'
        return context


class SkladbaDelete(DeleteView):
    model = Skladba
    name = model.__name__.casefold()
    template_name = 'intolc/delete.html'
    success_url = reverse_lazy(model.__name__.casefold())


class AlbumSkladbaCreate(CreateView):
    model = AlbumSkladba
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all()
        context['delete'] = f'{__class__.name}-delete'
        return context


class AlbumSkladbaDelete(DeleteView):
    model = AlbumSkladba
    name = model.__name__.casefold()
    template_name = 'intolc/delete.html'
    success_url = reverse_lazy(model.__name__.casefold())


class TypNarodnostCreate(CreateView):
    model = TypNarodnost
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all().order_by('nazev')
        context['delete'] = f'{__class__.name}-delete'
        return context


class TypNarodnostDelete(DeleteView):
    model = TypNarodnost
    template_name = 'intolc/delete.html'
    success_url = reverse_lazy(model.__name__.casefold())


class InterpretCreate(CreateView):
    model = Interpret
    fields = '__all__'
    name = model.__name__.casefold()
    template_name = f'intolc/add_form.html'
    success_url = f'/{name}/add'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = __class__.model.objects.all().order_by('nazev')
        context['delete'] = f'{__class__.name}-delete'
        return context


class InterpretDelete(DeleteView):
    model = Interpret
    template_name = 'intolc/delete.html'
    success_url = reverse_lazy(model.__name__.casefold())

