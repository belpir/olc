from django.db import models
from django.urls import reverse

# Create your models here.

class TypZanr(models.Model):
	nazev = models.CharField(max_length=50, unique=True)

	def __str__(self):
		return self.nazev

# 	def get_absolute_url(self):
#		return reverse('typ-zanr-detail', kwargs={'pk': self.pk})


class Album(models.Model):
	typ_zanr = models.ForeignKey(TypZanr, on_delete=models.CASCADE)
	nazev = models.CharField(max_length=50)
	datum_vydani = models.DateField(blank=True)

	class Meta:
		ordering = ['nazev', '-datum_vydani']

	def __str__(self):
		return self.nazev


class Skladba(models.Model):
	nazev = models.CharField(max_length=50)
	delka = models.CharField(max_length=8)
	# album = models.ManyToManyField(Album, through='AlbumSkladba', blank=True)

	def __str__(self):
		return self.nazev


class AlbumSkladba(models.Model):
	cislo_stopy = models.PositiveSmallIntegerField()
	album = models.ForeignKey(Album, on_delete=models.CASCADE)
	skladba = models.ForeignKey(Skladba, on_delete=models.CASCADE)

	class Meta:
		ordering = ['cislo_stopy']

	def __str__(self):
		return f'{self.cislo_stopy}'


class TypNarodnost(models.Model):
	nazev = models.CharField(max_length=20)

	def __str__(self):
		return self.nazev


class Interpret(models.Model):
	nazev = models.CharField(max_length=50)
	typ_narodnost = models.ForeignKey(TypNarodnost, on_delete=models.CASCADE)
	album = models.ManyToManyField(Album)

	class Meta:
		ordering = ['nazev']

	def __str__(self):
		return self.nazev

