"""olc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from intolc.views import *

urlpatterns = [
	path('', IndexView.as_view(template_name='intolc/index.html'), name='index'),
	path('typzanr/add/', TypZanrCreate.as_view(), name='typzanr'),
	path('typzanr/<int:pk>/delete', TypZanrDelete.as_view(), name='typzanr-delete'),
	path('album/add/', AlbumCreate.as_view(), name='album'),
	path('album/<int:pk>/delete', AlbumDelete.as_view(), name='album-delete'),
	path('skladba/add/', SkladbaCreate.as_view(), name='skladba'),
	path('skladba/<int:pk>/delete', SkladbaDelete.as_view(), name='skladba-delete'),
	path('albumskladba/<int:pk>/delete', SkladbaDelete.as_view(), name='skladba-delete'),
	path('albumskladba/add/', AlbumSkladbaCreate.as_view(), name='albumskladba'),
	path('albumskladba/<int:pk>/delete', AlbumSkladbaDelete.as_view(), name='albumskladba-delete'),
	path('typnarodnost/add/', TypNarodnostCreate.as_view(), name='typnarodnost'),
	path('TypNarodnost/<int:pk>/delete', TypNarodnostDelete.as_view(), name='typnarodnost-delete'),
	path('interpret/add/', InterpretCreate.as_view(), name='interpret'),
	path('interpret/<int:pk>/delete', InterpretDelete.as_view(), name='interpret-delete'),
]
